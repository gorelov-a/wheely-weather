package com.gorelov.anton.wheely.weather

import android.app.Application
import com.gorelov.anton.wheely.weather.di.DI

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        DI.init(this)
    }
}