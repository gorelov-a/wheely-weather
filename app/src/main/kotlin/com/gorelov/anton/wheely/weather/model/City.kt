package com.gorelov.anton.wheely.weather.model

data class City(
    val id: String,
    val lat: Double,
    val lon: Double,
    val radius: Double,
    val name: String
)