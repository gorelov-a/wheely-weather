package com.gorelov.anton.wheely.weather.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gorelov.anton.wheely.weather.database.entity.MessageEntity
import io.reactivex.Observable

@Dao
interface MessageDao {

    @Query("SELECT * FROM message")
    fun getAll(): Observable<List<MessageEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(messageEntity: MessageEntity): Long?

    @Query("DELETE FROM message")
    fun deleteAll()
}