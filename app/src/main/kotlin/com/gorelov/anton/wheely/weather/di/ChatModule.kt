package com.gorelov.anton.wheely.weather.di

import com.gorelov.anton.wheely.weather.model.CityId
import toothpick.config.Module

class ChatModule(cityId: CityId) : Module() {
    init {
        bind(CityId::class.java).toInstance(cityId)
    }
}