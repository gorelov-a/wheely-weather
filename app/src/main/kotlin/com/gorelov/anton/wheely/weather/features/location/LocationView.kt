package com.gorelov.anton.wheely.weather.features.location

import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.gorelov.anton.wheely.weather.common.BaseView
import com.gorelov.anton.wheely.weather.features.location.vm.CityListItemVM

@StateStrategyType(AddToEndStrategy::class)
interface LocationView : BaseView {

    @StateStrategyType(SingleStateStrategy::class)
    fun showCities(cities: List<CityListItemVM>)

    fun setRefreshing(isRefreshing: Boolean)

    fun setMenuEnabled(enabled: Boolean)

    fun openChatScreen(selectedCityId: String)
}