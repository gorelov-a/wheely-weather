package com.gorelov.anton.wheely.weather.common

import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar

abstract class BaseFragment : MvpAppCompatFragment(), BaseView {

    override fun showMessage(@StringRes stringId: Int) {
        val view = activity?.findViewById<View>(android.R.id.content) ?: return
        Snackbar.make(view, stringId, Snackbar.LENGTH_LONG).show()
    }
}