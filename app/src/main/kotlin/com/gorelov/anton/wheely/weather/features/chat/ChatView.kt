package com.gorelov.anton.wheely.weather.features.chat

import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.gorelov.anton.wheely.weather.common.BaseView
import com.gorelov.anton.wheely.weather.features.chat.vm.MessageVM

@StateStrategyType(AddToEndStrategy::class)
interface ChatView : BaseView {

    fun showMessages(messages: List<MessageVM>)

    fun showMessage(messageVM: MessageVM)

    fun clearInput()

    fun openLoginScreen()
}