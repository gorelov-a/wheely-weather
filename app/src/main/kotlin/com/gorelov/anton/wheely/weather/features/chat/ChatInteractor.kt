package com.gorelov.anton.wheely.weather.features.chat

import com.gorelov.anton.wheely.weather.database.MessageRepository
import com.gorelov.anton.wheely.weather.model.CityId
import com.gorelov.anton.wheely.weather.model.Message
import com.gorelov.anton.wheely.weather.network.WheelyEndpoint
import io.reactivex.Observable
import java.util.*
import javax.inject.Inject

class ChatInteractor @Inject constructor(
    private val messageRepository: MessageRepository,
    private val wheelyEndpoint: WheelyEndpoint
) {

    fun getWeather(cityId: CityId): Observable<Int> = wheelyEndpoint.getWeather()
        .map { response ->
            val calendar = Calendar.getInstance()
            var dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)

            if (dayOfWeek + 1 > 6) dayOfWeek = 0

            response.weather.find { it.cityId == cityId.id }?.temperatures?.get(dayOfWeek)
        }

    fun save(message: Message) = messageRepository.save(message)

    fun getMessages() = messageRepository.getAllMessages()

    fun deleteMessages() = messageRepository.deleteAll()

}