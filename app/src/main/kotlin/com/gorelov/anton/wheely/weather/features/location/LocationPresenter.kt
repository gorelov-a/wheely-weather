package com.gorelov.anton.wheely.weather.features.location

import com.arellomobile.mvp.InjectViewState
import com.gorelov.anton.wheely.weather.R
import com.gorelov.anton.wheely.weather.common.BasePresenter
import com.gorelov.anton.wheely.weather.common.utils.WheelySharedPreferences
import com.gorelov.anton.wheely.weather.di.DI
import com.gorelov.anton.wheely.weather.features.location.vm.CityItemConverter
import com.gorelov.anton.wheely.weather.model.City
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@InjectViewState
class LocationPresenter @Inject constructor(
    private val locationInteractor: LocationInteractor,
    private val sharedPreferences: WheelySharedPreferences
) : BasePresenter<LocationView>() {

    private var selectedCityId: String? = null
    private lateinit var cities: List<City>


    override fun onFirstViewAttach() {
        viewState.setRefreshing(true)
        disposable.add(
            locationInteractor
                .getCities()
                .map {
                    cities = it
                    CityItemConverter.from(it)
                }
                .doAfterNext {
                    if (selectedCityId != null) {
                        val selectedCity = it.find { it.id == selectedCityId }
                        if (selectedCity != null) {
                            selectedCity.isSelected = true
                        } else {
                            selectedCityId == null
                        }
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.setRefreshing(false)
                    viewState.showCities(it)
                    viewState.setMenuEnabled(selectedCityId != null)
                }, {
                    viewState.setRefreshing(false)
                    viewState.showCities(listOf())
                    viewState.setMenuEnabled(false)
                    viewState.showMessage(R.string.load_cities_error)
                })
        )
    }

    fun onRefresh() = onFirstViewAttach()

    fun onCitySelect(cityId: String) {
        selectedCityId = cityId
        disposable.add(
            Observable.fromCallable { cities }
                .subscribeOn(Schedulers.computation())
                .map { CityItemConverter.from(it) }
                .doAfterNext { it.find { it.id == selectedCityId }?.isSelected = true }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewState.showCities(it)
                    viewState.setMenuEnabled(true)
                }
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        DI.closeLocationScope()
    }

    fun onNextButtonClick() {
        val cityId = selectedCityId
        if (cityId != null) {
            sharedPreferences.saveCityId(cityId)
            viewState.openChatScreen(cityId)
        }
    }

}