package com.gorelov.anton.wheely.weather.features.chat

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gorelov.anton.wheely.weather.R
import com.gorelov.anton.wheely.weather.WheelyActivity
import com.gorelov.anton.wheely.weather.common.BaseFragment
import com.gorelov.anton.wheely.weather.common.exception.MissingCityIdException
import com.gorelov.anton.wheely.weather.di.DI
import com.gorelov.anton.wheely.weather.features.chat.vm.MessageVM
import com.gorelov.anton.wheely.weather.model.CityId
import kotlinx.android.synthetic.main.fragment_chat.*
import toothpick.Scope

class ChatFragment : BaseFragment(), ChatView {

    companion object {
        private const val CITY_ID_KEY = "city_id"

        fun openChatFragment(cityId: String): ChatFragment {
            val bundle = Bundle()
            bundle.putString(CITY_ID_KEY, cityId)
            return ChatFragment().apply {
                arguments = bundle
            }
        }
    }

    private lateinit var scope: Scope

    @InjectPresenter
    lateinit var chatPresenter: ChatPresenter

    @ProvidePresenter
    fun provideChatPresenter(): ChatPresenter = scope.getInstance(ChatPresenter::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        scope = DI.openChatScope(CityId(arguments?.getString(CITY_ID_KEY) ?: throw MissingCityIdException()))
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        (activity as WheelyActivity).supportActionBar?.apply {
            title = ""
            setDisplayHomeAsUpEnabled(false)
            setHomeButtonEnabled(false)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        chat.layoutManager = LinearLayoutManager(context)
        send_button.setOnClickListener {
            chatPresenter.onSendButtonClick(input.text.toString())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_chat, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_exit -> {
                chatPresenter.onExitMenuClick()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showMessages(messages: List<MessageVM>) {
        val context = this.context ?: throw Exception()
        chat.adapter = ChatAdapter(context, messages)
    }

    override fun showMessage(messageVM: MessageVM) = (chat.adapter as ChatAdapter).addMessage(messageVM)

    override fun clearInput() = input.text.clear()

    override fun openLoginScreen() {
        (activity as WheelyActivity).openLoginScreen()
    }
}