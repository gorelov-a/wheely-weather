package com.gorelov.anton.wheely.weather.features.location

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gorelov.anton.wheely.weather.R
import com.gorelov.anton.wheely.weather.WheelyActivity
import com.gorelov.anton.wheely.weather.common.BaseFragment
import com.gorelov.anton.wheely.weather.di.DI
import com.gorelov.anton.wheely.weather.features.location.vm.CityListItemVM
import kotlinx.android.synthetic.main.fragment_location.*


class LocationFragment : BaseFragment(), LocationView {

    private val scope by lazy { DI.openLocationScope() }
    private var isMenuEnabled = false

    @InjectPresenter
    lateinit var locationPresenter: LocationPresenter

    @ProvidePresenter
    fun provideLocationPresenter(): LocationPresenter = scope.getInstance(LocationPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        (activity as WheelyActivity).supportActionBar?.apply {
            setTitle(R.string.city_select)
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_location, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_location, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_continue -> {
                locationPresenter.onNextButtonClick()
                true
            }
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        menu?.getItem(0)?.isEnabled = isMenuEnabled
        super.onPrepareOptionsMenu(menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cities_list.layoutManager = LinearLayoutManager(context)
        swipe_refresh_layout.setOnRefreshListener {
            locationPresenter.onRefresh()
        }
    }

    override fun showCities(cities: List<CityListItemVM>) {
        val context = this.context ?: throw Exception()
        cities_list.adapter = LocationAdapter(context, cities) {
            locationPresenter.onCitySelect(it.id)
        }
    }

    override fun setRefreshing(isRefreshing: Boolean) {
        swipe_refresh_layout.isRefreshing = isRefreshing
    }

    override fun setMenuEnabled(enabled: Boolean) {
        isMenuEnabled = enabled
        activity?.invalidateOptionsMenu()
    }

    override fun openChatScreen(selectedCityId: String) {
        (activity as WheelyActivity).openChatScreen(selectedCityId)
    }

}