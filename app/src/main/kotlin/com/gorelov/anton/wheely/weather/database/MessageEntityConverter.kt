package com.gorelov.anton.wheely.weather.database

import com.gorelov.anton.wheely.weather.database.entity.MessageEntity
import com.gorelov.anton.wheely.weather.model.Message

object MessageEntityConverter {

    fun from(messages: List<MessageEntity>): List<Message> = messages.mapTo(mutableListOf()) { from(it) }

    fun from(message: Message): MessageEntity = MessageEntity(
        message.id,
        message.isFromUser,
        message.text
    )

    fun from(message: MessageEntity): Message = Message(
        message.id,
        message.is_from_user,
        message.text
    )
}