package com.gorelov.anton.wheely.weather.features.location

import com.gorelov.anton.wheely.weather.model.City
import com.gorelov.anton.wheely.weather.model.CityId
import com.gorelov.anton.wheely.weather.network.CityItemDtoConverter
import com.gorelov.anton.wheely.weather.network.WheelyEndpoint
import io.reactivex.Observable
import javax.inject.Inject

class LocationInteractor @Inject constructor(
    private val wheelyEndpoint: WheelyEndpoint
) {

    fun getCities(): Observable<List<City>> = wheelyEndpoint
        .getCities()
        .map { CityItemDtoConverter.from(it.cities) }

    fun getCityById(cityId: CityId): Observable<City> = wheelyEndpoint
        .getCities()
        .flatMap { Observable.fromIterable(CityItemDtoConverter.from(it.cities)) }
        .filter { it.id == cityId.id }
}