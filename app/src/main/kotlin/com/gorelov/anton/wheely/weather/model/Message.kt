package com.gorelov.anton.wheely.weather.model

data class Message(
    val id: Long?,
    val isFromUser: Boolean,
    val text: String
)