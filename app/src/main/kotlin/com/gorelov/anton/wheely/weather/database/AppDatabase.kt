package com.gorelov.anton.wheely.weather.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gorelov.anton.wheely.weather.database.dao.MessageDao
import com.gorelov.anton.wheely.weather.database.entity.MessageEntity

@Database(version = 1, entities = [MessageEntity::class])
abstract class AppDatabase : RoomDatabase() {

    abstract fun messageDao(): MessageDao
}