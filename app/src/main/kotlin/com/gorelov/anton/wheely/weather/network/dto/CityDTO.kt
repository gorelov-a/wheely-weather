package com.gorelov.anton.wheely.weather.network.dto

import com.google.gson.annotations.SerializedName

data class CityDTO(
    @SerializedName("id") val id: String,
    @SerializedName("center") val center: List<Double>,
    @SerializedName("radius") val radius: Double,
    @SerializedName("name") val name: String
)