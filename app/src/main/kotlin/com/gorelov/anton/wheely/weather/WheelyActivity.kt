package com.gorelov.anton.wheely.weather

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.gorelov.anton.wheely.weather.common.MvpAppCompatFragment
import com.gorelov.anton.wheely.weather.common.utils.WheelySharedPreferences
import com.gorelov.anton.wheely.weather.features.chat.ChatFragment
import com.gorelov.anton.wheely.weather.features.location.LocationFragment
import com.gorelov.anton.wheely.weather.features.login.LoginFragment

class WheelyActivity : AppCompatActivity() {

    private val sharedPreferences by lazy {
        WheelySharedPreferences(this.applicationContext)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            when {
                isCityIdSetup() -> openChatScreen(sharedPreferences.getCityId())
                isUserNameSetup() -> openLocationScreen()
                else -> openLoginScreen()
            }
        }
    }

    fun openLoginScreen() = replaceFragment(LoginFragment())

    fun openLocationScreen() = replaceFragment(LocationFragment())

    fun openChatScreen(cityId: String) = replaceFragment(ChatFragment.openChatFragment(cityId))

    private fun isUserNameSetup() = !sharedPreferences.getUserName().isBlank()

    private fun isCityIdSetup() = !sharedPreferences.getCityId().isBlank()

    override fun onBackPressed() {
        val fragment = supportFragmentManager.fragments[0]
        when (fragment) {
            is LoginFragment -> super.onBackPressed()
            is LocationFragment -> replaceFragment(LoginFragment())
            is ChatFragment -> openHomeScreen()
        }
    }

    private fun openHomeScreen() {
        val homeIntent = Intent(Intent.ACTION_MAIN)
        homeIntent.addCategory(Intent.CATEGORY_HOME)
        homeIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(homeIntent)
    }

    private fun replaceFragment(fragment: MvpAppCompatFragment) {
        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction()
            .replace(R.id.frame_container, fragment as Fragment)
            .commit()
    }
}
