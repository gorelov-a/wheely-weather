package com.gorelov.anton.wheely.weather.features.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.gorelov.anton.wheely.weather.R
import com.gorelov.anton.wheely.weather.features.chat.vm.MessageVM

class ChatAdapter(
    context: Context,
    messages: List<MessageVM>
) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val messages: MutableList<MessageVM> = mutableListOf()

    init {
        this.messages.addAll(messages)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(inflater.inflate(viewType, parent, false))

    override fun getItemViewType(position: Int): Int =
        if (messages[position].isFromUser) R.layout.view_chat_item_right else R.layout.view_chat_item_left

    override fun getItemCount(): Int = messages.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(messages[position])
    }

    fun addMessage(message: MessageVM) {
        messages.add(message)
        notifyItemInserted(messages.lastIndex)
    }

    inner class ViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        private val messageText: TextView = itemView.findViewById(R.id.message_text)

        fun bind(message: MessageVM) {
            messageText.text = message.text
        }
    }
}