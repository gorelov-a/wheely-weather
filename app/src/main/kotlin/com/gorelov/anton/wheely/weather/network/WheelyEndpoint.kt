package com.gorelov.anton.wheely.weather.network

import com.gorelov.anton.wheely.weather.network.dto.CitiesListResponse
import com.gorelov.anton.wheely.weather.network.dto.WeatherResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface WheelyEndpoint {

    @GET("/static.wheely.com/android-test/cities.json")
    fun getCities(): Observable<CitiesListResponse>

    @GET("/static.wheely.com/android-test/weather.json")
    fun getWeather(): Observable<WeatherResponse>
}