package com.gorelov.anton.wheely.weather.features.location.vm

import com.gorelov.anton.wheely.weather.model.City

object CityItemConverter {

    fun from(cities: List<City>): List<CityListItemVM> = cities.mapTo(mutableListOf()) { from(it) }

    private fun from(city: City): CityListItemVM = CityListItemVM(
        city.id,
        city.name,
        false
    )
}