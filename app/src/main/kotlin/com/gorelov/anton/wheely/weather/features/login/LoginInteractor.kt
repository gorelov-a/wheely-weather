package com.gorelov.anton.wheely.weather.features.login

import com.gorelov.anton.wheely.weather.common.utils.WheelySharedPreferences
import javax.inject.Inject

class LoginInteractor @Inject constructor(private val sharedPreferences: WheelySharedPreferences) {

    fun saveUserName(userName: String) = sharedPreferences.saveUserName(userName)

}