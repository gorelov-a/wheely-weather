package com.gorelov.anton.wheely.weather

object AppConstans {
    const val SHARED_PREFERENCES_NAME = "wheely_pref"
    const val USER_NAME = "user_name"
    const val CITY_ID = "city_id"
    const val WEATHER = "weather"
}