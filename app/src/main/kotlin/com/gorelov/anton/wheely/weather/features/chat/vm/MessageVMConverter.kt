package com.gorelov.anton.wheely.weather.features.chat.vm

import com.gorelov.anton.wheely.weather.model.Message

object MessageVMConverter {
    fun from(messages: List<Message>): List<MessageVM> = messages.mapTo(mutableListOf()) { from(it) }

    fun from(message: Message): MessageVM = MessageVM(
        message.id,
        message.isFromUser,
        message.text
    )
}