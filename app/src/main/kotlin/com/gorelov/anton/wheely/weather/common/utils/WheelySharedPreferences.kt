package com.gorelov.anton.wheely.weather.common.utils

import android.content.Context
import com.gorelov.anton.wheely.weather.AppConstans.CITY_ID
import com.gorelov.anton.wheely.weather.AppConstans.SHARED_PREFERENCES_NAME
import com.gorelov.anton.wheely.weather.AppConstans.USER_NAME
import com.gorelov.anton.wheely.weather.AppConstans.WEATHER
import javax.inject.Inject

class WheelySharedPreferences @Inject constructor(context: Context) {

    private val sharedPreferences by lazy {
        context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE)
    }

    fun getUserName(): String = sharedPreferences.getString(USER_NAME, null) ?: ""

    fun saveUserName(userName: String) {
        sharedPreferences.edit()
            .putString(USER_NAME, userName)
            .apply()
    }

    fun deleteUserName() = sharedPreferences.edit().remove(USER_NAME).apply()

    fun getCityId(): String = sharedPreferences.getString(CITY_ID, null) ?: ""

    fun saveCityId(cityId: String) {
        sharedPreferences.edit()
            .putString(CITY_ID, cityId)
            .apply()
    }

    fun deleteCityId() = sharedPreferences.edit().remove(CITY_ID).apply()

    fun getWeather(): Int = sharedPreferences.getInt(WEATHER, Int.MIN_VALUE)

    fun containsWeather(): Boolean = sharedPreferences.contains(WEATHER)

    fun saveWeather(weather: Int) {
        sharedPreferences.edit()
            .putInt(WEATHER, weather)
            .apply()
    }

    fun deleteWeather() = sharedPreferences.edit().remove(WEATHER).apply()
}