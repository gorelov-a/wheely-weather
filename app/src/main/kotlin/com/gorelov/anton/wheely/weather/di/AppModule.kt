package com.gorelov.anton.wheely.weather.di

import android.content.Context
import androidx.room.Room
import com.gorelov.anton.wheely.weather.common.utils.ResourcesProvider
import com.gorelov.anton.wheely.weather.database.AppDatabase
import toothpick.config.Module

class AppModule(applicationContext: Context) : Module() {
    init {
        bind(Context::class.java).toInstance(applicationContext)
        bind(ResourcesProvider::class.java).to(ResourcesProvider::class.java).singletonInScope()
        bind(AppDatabase::class.java).toInstance(
            Room.databaseBuilder(applicationContext, AppDatabase::class.java, "database")
                .allowMainThreadQueries()
                .build()
        )
    }
}