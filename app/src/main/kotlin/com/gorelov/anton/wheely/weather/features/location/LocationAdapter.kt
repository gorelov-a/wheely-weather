package com.gorelov.anton.wheely.weather.features.location

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.gorelov.anton.wheely.weather.R
import com.gorelov.anton.wheely.weather.features.location.vm.CityListItemVM

class LocationAdapter(
    context: Context,
    private val cities: List<CityListItemVM>,
    private val clickListener: (data: CityListItemVM) -> Unit
) : RecyclerView.Adapter<LocationAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.view_city_item, parent, false), clickListener)
    }

    override fun getItemCount(): Int = cities.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(cities[position])
    }

    inner class ViewHolder(
        itemView: View,
        listener: (data: CityListItemVM) -> Unit
    ) : RecyclerView.ViewHolder(itemView) {

        private val cityRadioButton: RadioButton = itemView.findViewById(R.id.city)

        init {
            cityRadioButton.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    listener.invoke(cities[adapterPosition])
                }
            }
        }

        fun bind(city: CityListItemVM) {
            cityRadioButton.text = city.name
            cityRadioButton.isChecked = city.isSelected
        }
    }
}
