package com.gorelov.anton.wheely.weather.di

import com.gorelov.anton.wheely.weather.network.WheelyApiProvider
import com.gorelov.anton.wheely.weather.network.WheelyEndpoint
import toothpick.config.Module

class NetworkModule : Module() {
    init {
        bind(WheelyEndpoint::class.java).toProvider(WheelyApiProvider::class.java).providesSingletonInScope()
    }
}