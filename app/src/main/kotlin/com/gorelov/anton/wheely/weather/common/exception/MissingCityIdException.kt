package com.gorelov.anton.wheely.weather.common.exception

class MissingCityIdException : Throwable()
