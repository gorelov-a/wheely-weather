package com.gorelov.anton.wheely.weather.features.login

import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.gorelov.anton.wheely.weather.common.BaseView

@StateStrategyType(AddToEndStrategy::class)
interface LoginView : BaseView {

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showNameIsBlankError()

    fun openLocationScreen()

}