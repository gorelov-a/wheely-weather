package com.gorelov.anton.wheely.weather.network

object NetworkConsts {
    const val baseUrl = "https://cdn.wheely.com/"
    const val timeoutInSeconds = 2L
}