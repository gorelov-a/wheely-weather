package com.gorelov.anton.wheely.weather.network

import com.gorelov.anton.wheely.weather.model.City
import com.gorelov.anton.wheely.weather.network.dto.CityDTO

object CityItemDtoConverter {

    fun from(cities: List<CityDTO>): List<City> = cities.mapTo(mutableListOf()) { from(it) }

    private fun from(city: CityDTO): City = City(
        city.id,
        city.center[0],
        city.center[1],
        city.radius,
        city.name
    )
}