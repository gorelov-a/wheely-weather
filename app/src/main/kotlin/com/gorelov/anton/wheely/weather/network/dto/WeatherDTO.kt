package com.gorelov.anton.wheely.weather.network.dto

import com.google.gson.annotations.SerializedName

data class WeatherDTO(
    @SerializedName("city_id") val cityId: String,
    @SerializedName("temperatures") val temperatures: List<Int>
)