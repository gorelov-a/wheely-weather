package com.gorelov.anton.wheely.weather.network.dto

import com.google.gson.annotations.SerializedName

data class CitiesListResponse(@SerializedName("cities") val cities: List<CityDTO>)