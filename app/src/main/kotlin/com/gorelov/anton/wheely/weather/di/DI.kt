package com.gorelov.anton.wheely.weather.di

import android.content.Context
import com.gorelov.anton.wheely.weather.BuildConfig
import com.gorelov.anton.wheely.weather.model.CityId
import toothpick.Scope
import toothpick.Toothpick
import toothpick.configuration.Configuration
import toothpick.registries.FactoryRegistryLocator
import toothpick.registries.MemberInjectorRegistryLocator

object DI {

    private const val APP_SCOPE = "app_scope"
    private const val LOGIN_SCOPE = "login_scope"
    private const val LOCATION_SCOPE = "location_scope"
    private const val CHAT_SCOPE = "chat_scope"

    private val openScopes: MutableCollection<String> = HashSet()

    fun init(applicationContext: Context) {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction().disableReflection());
            MemberInjectorRegistryLocator.setRootRegistry(com.gorelov.anton.wheely.weather.MemberInjectorRegistry())
            FactoryRegistryLocator.setRootRegistry(com.gorelov.anton.wheely.weather.FactoryRegistry())
        }

        Toothpick.openScopes(APP_SCOPE).apply {
            installModules(AppModule(applicationContext), NetworkModule())
        }
    }

    fun openLoginScope(): Scope = Toothpick.openScopes(APP_SCOPE, LOGIN_SCOPE)

    fun closeLoginScope() = Toothpick.closeScope(LOGIN_SCOPE)

    fun openLocationScope(): Scope = Toothpick.openScopes(APP_SCOPE, LOCATION_SCOPE)

    fun closeLocationScope() = Toothpick.closeScope(LOCATION_SCOPE)

    fun openChatScope(cityId: CityId): Scope {
        return Toothpick.openScopes(APP_SCOPE, CHAT_SCOPE).apply {
            if (!openScopes.contains(CHAT_SCOPE)) {
                openScopes.add(CHAT_SCOPE)
                installModules(ChatModule(cityId))
            }
        }
    }

    fun closeChatScope() {
        openScopes.remove(CHAT_SCOPE)
        Toothpick.closeScope(CHAT_SCOPE)
    }
}