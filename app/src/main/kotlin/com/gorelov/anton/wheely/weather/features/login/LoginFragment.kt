package com.gorelov.anton.wheely.weather.features.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gorelov.anton.wheely.weather.R
import com.gorelov.anton.wheely.weather.WheelyActivity
import com.gorelov.anton.wheely.weather.common.BaseFragment
import com.gorelov.anton.wheely.weather.di.DI
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : BaseFragment(), LoginView {

    private val scope by lazy { DI.openLoginScope() }

    @InjectPresenter
    lateinit var loginPresenter: LoginPresenter

    @ProvidePresenter
    fun provideLoginPresenter(): LoginPresenter = scope.getInstance(LoginPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(false)
        (activity as WheelyActivity).supportActionBar?.apply {
            setTitle(R.string.app_name)
            setDisplayHomeAsUpEnabled(false)
            setHomeButtonEnabled(false)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button.setOnClickListener {
            loginPresenter.onNextButtonClick(editText.text.toString())
        }
    }

    override fun openLocationScreen() = (activity as WheelyActivity).openLocationScreen()

    override fun showNameIsBlankError() = showMessage(R.string.error_name_is_empty)
}