package com.gorelov.anton.wheely.weather.features.login

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gorelov.anton.wheely.weather.di.DI
import javax.inject.Inject

@InjectViewState
class LoginPresenter @Inject constructor(private val loginInteractor: LoginInteractor) : MvpPresenter<LoginView>() {

    fun onNextButtonClick(userName: String) {
        if (userName.isBlank()) {
            viewState.showNameIsBlankError()
        } else {
            loginInteractor.saveUserName(userName)
            viewState.openLocationScreen()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        DI.closeLoginScope()
    }
}