package com.gorelov.anton.wheely.weather.network.dto

import com.google.gson.annotations.SerializedName

data class WeatherResponse(@SerializedName("weather") val weather: List<WeatherDTO>)