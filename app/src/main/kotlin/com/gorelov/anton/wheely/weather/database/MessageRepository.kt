package com.gorelov.anton.wheely.weather.database

import com.gorelov.anton.wheely.weather.model.Message
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class MessageRepository @Inject constructor(private val appDatabase: AppDatabase) {

    fun getAllMessages(): Observable<List<Message>> =
        appDatabase.messageDao().getAll().map { MessageEntityConverter.from(it) }

    fun save(message: Message): Observable<Message> {
        return Observable.just(
            appDatabase.messageDao()
                .insert(MessageEntityConverter.from(message))
        )
            .map { Message(it, message.isFromUser, message.text) }
    }

    fun deleteAll(): Completable = Completable.fromCallable(appDatabase.messageDao()::deleteAll)
}