package com.gorelov.anton.wheely.weather.features.chat

import com.arellomobile.mvp.InjectViewState
import com.gorelov.anton.wheely.weather.R
import com.gorelov.anton.wheely.weather.common.BasePresenter
import com.gorelov.anton.wheely.weather.common.utils.ResourcesProvider
import com.gorelov.anton.wheely.weather.common.utils.WheelySharedPreferences
import com.gorelov.anton.wheely.weather.di.DI
import com.gorelov.anton.wheely.weather.features.chat.vm.MessageVMConverter
import com.gorelov.anton.wheely.weather.features.location.LocationInteractor
import com.gorelov.anton.wheely.weather.model.CityId
import com.gorelov.anton.wheely.weather.model.Message
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

@InjectViewState
class ChatPresenter @Inject constructor(
    private val cityId: CityId,
    private val chatInteractor: ChatInteractor,
    private val locationInteractor: LocationInteractor,
    private val sharedPreferences: WheelySharedPreferences,
    private val resourcesProvider: ResourcesProvider
) : BasePresenter<ChatView>() {

    private val weather = AtomicInteger()

    override fun onFirstViewAttach() {
        if (sharedPreferences.containsWeather()) {
            weather.set(sharedPreferences.getWeather())
            disposable.add(
                chatInteractor.getMessages()
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { viewState.showMessages(MessageVMConverter.from(it)) },
                        { viewState.showMessage(R.string.error) }
                    )
            )
        } else {
            disposable.add(
                chatInteractor.getWeather(cityId)
                    .subscribeOn(Schedulers.io())
                    .doAfterNext { sharedPreferences.saveWeather(it) }
                    .doAfterNext { weather.set(it) }
                    .flatMap { locationInteractor.getCityById(cityId) }
                    .map { getFirstMessage(it.name) }
                    .flatMap { chatInteractor.save(it) }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { viewState.showMessages(MessageVMConverter.from(listOf(it))) },
                        { viewState.showMessage(R.string.error) }
                    )
            )
        }
    }

    fun onSendButtonClick(message: String) {
        if (message.isBlank()) {
            return
        }

        disposable.add(
            Observable.just(message)
                .subscribeOn(Schedulers.computation())
                .map { Message(null, true, message) }
                .flatMap { chatInteractor.save(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterNext { viewState.showMessage(MessageVMConverter.from(it)) }
                .doAfterNext { viewState.clearInput() }
                .observeOn(Schedulers.computation())
                .map { getAnswer(it) }
                .flatMap { chatInteractor.save(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { viewState.showMessage(MessageVMConverter.from(it)) },
                    { viewState.showMessage(R.string.error) }
                )
        )
    }

    fun onExitMenuClick() {
        sharedPreferences.deleteUserName()
        sharedPreferences.deleteCityId()
        sharedPreferences.deleteWeather()

        disposable.add(
            chatInteractor.deleteMessages()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewState.openLoginScreen()
                }
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        DI.closeChatScope()
    }

    private fun getFirstMessage(cityName: String): Message {
        val userName = sharedPreferences.getUserName()
        return Message(null, false, resourcesProvider.getString(R.string.first_message, userName, cityName))
    }

    private fun getAnswer(message: Message): Message {
        val weatherFromUser = message.text.toInt()
        val answer = when {
            weatherFromUser > weather.get() -> resourcesProvider.getString(R.string.answer_colder)
            weatherFromUser < weather.get() -> resourcesProvider.getString(R.string.answer_warmer)
            else -> resourcesProvider.getString(R.string.answer_done)
        }
        return Message(null, false, answer)
    }
}