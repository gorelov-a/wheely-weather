package com.gorelov.anton.wheely.weather.features.chat.vm

data class MessageVM(
    val id: Long?,
    val isFromUser: Boolean,
    val text: String
)