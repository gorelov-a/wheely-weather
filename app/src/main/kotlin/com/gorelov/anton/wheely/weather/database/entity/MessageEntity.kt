package com.gorelov.anton.wheely.weather.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "message")
data class MessageEntity(
    @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) val id: Long?,
    @ColumnInfo(name = "is_from_user") val is_from_user: Boolean,
    @ColumnInfo(name = "text") val text: String
)