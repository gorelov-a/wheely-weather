package com.gorelov.anton.wheely.weather.features.location.vm

data class CityListItemVM(
    val id: String,
    val name: String,
    var isSelected: Boolean
)